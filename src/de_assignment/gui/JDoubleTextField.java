package de_assignment.gui;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class JDoubleTextField extends JTextField {
    // Indicates if caret position needs to be preserved after setValue
    private boolean saveCaretPos = false;

    public JDoubleTextField(Double d, int columns) {
        super(String.valueOf(d), columns);
    }

    public JDoubleTextField() {
        ((PlainDocument) getDocument()).setDocumentFilter(new DoubleFilter());

        // Add Enter key handling (lose & regain focus, so the value is sent to the server
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() != KeyEvent.VK_ENTER)
                    return;

                saveCaretPos = true;
                transferFocus();
                grabFocus();
            }
        });
    }

    public Double getValue() {
        return Double.parseDouble(getText());
    }

    public void setValue(int value) {
        // If caret position needs to be preserved, save it
        int caretPos = 0;
        if (saveCaretPos) {
            saveCaretPos = false;
            caretPos = getCaretPosition();
        }

        setText(String.valueOf(value));

        // Try to set saved caret position
        try {
            setCaretPosition(caretPos);
        } catch (Exception ignored) {
        }
    }

    public void setValue(Integer value) {
        this.setValue((int) value);
    }

    private class DoubleFilter extends DocumentFilter {
        @Override
        public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
            Document      doc = fb.getDocument();
            StringBuilder sb  = new StringBuilder();
            sb.append(doc.getText(0, doc.getLength()));
            sb.insert(offset, string);

            if (test(sb.toString())) {
                super.insertString(fb, offset, string, attr);
            } else {
                // warn the user and don't allow the insert
            }
        }

        private boolean test(String text) {
            if (text.equals("") || text.equals("-"))
                return true;

            try {
                Double.parseDouble(text);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            Document      doc = fb.getDocument();
            StringBuilder sb  = new StringBuilder();
            sb.append(doc.getText(0, doc.getLength()));
            sb.replace(offset, offset + length, text);

            if (test(sb.toString())) {
                super.replace(fb, offset, length, text, attrs);
            } else {
                // warn the user and don't allow the insert
            }
        }

        @Override
        public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
            Document      doc = fb.getDocument();
            StringBuilder sb  = new StringBuilder();
            sb.append(doc.getText(0, doc.getLength()));
            sb.delete(offset, offset + length);

            if (test(sb.toString())) {
                super.remove(fb, offset, length);
            } else {
                // warn the user and don't allow the insert
            }
        }
    }
}
