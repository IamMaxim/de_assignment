package de_assignment.gui

import de_assignment.solver.init
import java.awt.Dimension
import javax.swing.JFrame
import javax.swing.WindowConstants

class MainWindow {
    companion object {
        fun show() {
            val form = MainForm()
            val content = form.panel1

            val window = JFrame("Differential Equation Solver")
            window.contentPane = content
            window.size = Dimension(700, 700)
            window.setLocationRelativeTo(null)
            window.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
            window.isVisible = true

            init(form)
        }
    }
}
