package de_assignment.gui

import org.knowm.xchart.XYChartBuilder
import org.knowm.xchart.XYSeries
import org.knowm.xchart.style.Styler
import java.awt.Graphics
import java.awt.Graphics2D
import javax.swing.JComponent

/** This class is just a Swing wrapper around XChart's XYChart.
 *
 * It sets the common style and default axes names for the plot.
 * */
class Graph(title: String) : JComponent() {
    val chart = XYChartBuilder()
            .title(title)
            .xAxisTitle("X")
            .yAxisTitle("Y")
            .theme(Styler.ChartTheme.Matlab)
            .build()

    init {
        chart.styler.defaultSeriesRenderStyle = XYSeries.XYSeriesRenderStyle.Line
        chart.styler.markerSize = 0
    }

    override fun paint(g: Graphics?) {
        super.paint(g)
        chart.paint(g as Graphics2D, width, height)
    }
}