package de_assignment.solver

/** This class describes a solution produced by the solver.
 *
 *    xs - array of x coordinates of the points
 *    approx_ys - approximated y coordinates produced by a particular method at a corresponding x coordinate.
 *    exact_ys - y coordinates produced by the exact solution at a corresponding x coordinate.
 *    local_error_ys - difference of total error between current and previous x points.
 *    total_error_ys - difference between approximated solution and exact solution at a corresponding point.
 * */
class Solution(
        val xs: DoubleArray,
        val approx_ys: DoubleArray,
        val exact_ys: DoubleArray,
        val local_error_ys: DoubleArray,
        val total_error_ys: DoubleArray
)

/** The base class for differential equqation solvers. Solver accepts the function f(x, y), exact solution function
 * and the IVP data in the form of Context class.
 * */
abstract class EquationSolver(val name: String) {
    abstract fun solve(x_0: Double, y_0: Double, x_max: Double, step: Double, f: (x: Double, y: Double) -> Double, analytical: (x: Double) -> Double): Solution
}