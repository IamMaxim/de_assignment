package de_assignment.solver

import de_assignment.gui.MainForm
import kotlin.math.abs
import kotlin.math.exp


/** Adds logic to the GUI form. Makes UI actually do something.
 * */
fun init(form: MainForm) {
    form.recalculateButton.addActionListener {
        // All solvers in one list to allow iteration over them
        val solvers = listOf(EulerMethod(), ImprovedEulerMethod(), RungeKuttaMethod())

        // The given f(x, y)
        val f = { x: Double, y: Double -> y / x - x - y }
        // The derived constant calculation function for exact solution
        val cf = { x_0: Double, y_0: Double -> (y_0 / x_0 + 1) * exp(x_0) }
        // The calculated constant for the current input data
        val c = cf(form.x_0.value, form.y_0.value)
        // The exact solution function itself
        val analytical = { x: Double -> x * (c / exp(x) - 1.0) }

        val x0 = form.x_0.value
        val y0 = form.y_0.value
        val xMax = form.x_max.value
        val n = form.n.value
        val step = (xMax - x0) / n

        // We have to process the exact solution separately, since it doesn't have error graphs.
        val exactSolution = solvers[0].solve(x0, y0, xMax, step, f, analytical)
        form.solution.chart.removeSeries("Exact")
        form.solution.chart.addSeries("Exact", exactSolution.xs, exactSolution.exact_ys)

        // Process the rest of solvers
        for (solver in solvers) {
            val solution = solver.solve(x0, y0, xMax, step, f, analytical)

            // Add the solution graph
            form.solution.chart.removeSeries(solver.name)
            form.solution.chart.addSeries(solver.name, solution.xs, solution.approx_ys)

            // Add error graphs
            form.local_error.chart.removeSeries(solver.name)
            form.local_error.chart.addSeries(solver.name, solution.xs, solution.local_error_ys)
            form.total_error.chart.removeSeries(solver.name)
            form.total_error.chart.addSeries(solver.name, solution.xs, solution.total_error_ys)

            // Calculate the max local error graph
            val localErrXs = DoubleArray(n.toInt())
            val localErrYs = DoubleArray(n.toInt())
            for (n1 in 1..n.toInt()) {
                val step1 = (xMax - x0) / n1
                val sol = solver.solve(x0, y0, xMax, step1, f, analytical)

                var maxLocalErr = 0.0
                for (localErrorY in sol.local_error_ys) {
                    if (abs(localErrorY) > maxLocalErr)
                        maxLocalErr = abs(localErrorY)
                }

                if (maxLocalErr == Double.POSITIVE_INFINITY || maxLocalErr == Double.NEGATIVE_INFINITY)
                    continue

                localErrXs[n1 - 1] = n1.toDouble()
                localErrYs[n1 - 1] = maxLocalErr
            }

            // Update the graph
            form.max_local_error.chart.removeSeries(solver.name)
            form.max_local_error.chart.addSeries(solver.name, localErrXs, localErrYs)
        }

        form.panel1.repaint()
    }
}