package de_assignment.solver

import de_assignment.main.step

class EulerMethod : EquationSolver("Euler") {
    override fun solve(x_0: Double,
                       y_0: Double,
                       x_max: Double,
                       step: Double,
                       f: (x: Double, y: Double) -> Double,
                       analytical: (x: Double) -> Double): Solution {
        val values = ArrayList<Value>()

        // Insert IVP data
        values.add(Value(
                x = x_0,
                y = y_0,
                y_A = y_0,
                e_L = 0.0,
                e_G = 0.0
        ))

        for (x in x_0..x_max step step) {
            // Skip the IVP step, as it is already processed
            // Also, skip point of discontinuity
            if (x == x_0 || x == 0.0)
                continue

            val prev = values[values.size - 1]

            // Do Euler method calculations
            val analyticalSol = analytical(x)
            val f1 = f(prev.x, prev.y_A)
            val y_A = prev.y_A + step * f1
            val e_g = y_A - analyticalSol

            // Skip infinity points
            if (analyticalSol == Double.POSITIVE_INFINITY ||
                    y_A == Double.POSITIVE_INFINITY ||
                    e_g == Double.POSITIVE_INFINITY ||
                    e_g - prev.e_G == Double.POSITIVE_INFINITY)
                continue
            if (analyticalSol == Double.NEGATIVE_INFINITY ||
                    y_A == Double.NEGATIVE_INFINITY ||
                    e_g == Double.NEGATIVE_INFINITY ||
                    e_g - prev.e_G == Double.NEGATIVE_INFINITY)
                continue

            values.add(Value(
                    x = x,
                    y = analyticalSol,
                    y_A = y_A,
                    e_L = e_g - prev.e_G,
                    e_G = e_g
            ))
        }

        // Create solution filled with empty data
        val sol = Solution(
                xs = DoubleArray(values.size) { 0.0 },
                approx_ys = DoubleArray(values.size) { 0.0 },
                exact_ys = DoubleArray(values.size) { 0.0 },
                local_error_ys = DoubleArray(values.size) { 0.0 },
                total_error_ys = DoubleArray(values.size) { 0.0 }
        )

        // Populate solution with the actual data
        values.forEachIndexed { index, value ->
            sol.xs[index] = value.x
            sol.approx_ys[index] = value.y_A
            sol.exact_ys[index] = value.y
            sol.local_error_ys[index] = value.e_L
            sol.total_error_ys[index] = value.e_G
        }

        return sol
    }
}