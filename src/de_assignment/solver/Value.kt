package de_assignment.solver

class Value(
        val x: Double, // x of solution
        val y: Double, // y of exact solution
        val y_A: Double, // y approximate
        val e_L: Double, // local error
        val e_G: Double // global error
) {
    override fun toString(): String {
        return "Value(x=$x, y=$y, y_A=$y_A, e_L=$e_L, e_G=$e_G)"
    }
}