package de_assignment.main

infix fun ClosedRange<Double>.step(step: Double): Iterable<Double> {
    require(start.isFinite())
    require(endInclusive.isFinite())
    require(step != 0.0) { "Step must not be zero, was: $step." }

    if (step > 0.0)
        return generateSequence(start) { previous ->
            if (previous == Double.POSITIVE_INFINITY) return@generateSequence null
            val next = previous + step
            if (next > endInclusive) null else next
        }.asIterable()
    else
        return generateSequence(start) { previous ->
            if (previous == Double.NEGATIVE_INFINITY) return@generateSequence null
            val next = previous + step
            if (next < endInclusive) null else next
        }.asIterable()
}
