package de_assignment.main

import de_assignment.gui.MainWindow


fun main() {
    println("Starting DE equation de_assignment.solver...")
    MainWindow.show()
}